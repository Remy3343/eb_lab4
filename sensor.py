import math
'''
Get the 3-axis tilt degree from sensor

params: previous angles from x-axis, y-axis, z-axis
return: update angles

'''

# You have to read data and process them from sensor
# Hint: pitch, roll
# Be careful for the unit of measurement, the input/output format is "degree"

def getTiltDegree(prev_angleX, prev_angleY, prev_angleZ):
    angleX, angleY, angleZ = 0, 0, 0
    
    # read and process data
    return angleX, angleY, angleZ	